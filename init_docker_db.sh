#!/bin/sh

echo Initializing Docker database...

docker exec -it cron-pg_postgresql_1 /bin/bash /usr/src/scripts/create_prod_db.sh
docker exec -it cron-pg_backend_1  /bin/bash scripts/init_prod_db.sh
