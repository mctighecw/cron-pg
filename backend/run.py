import os
from src.app import create_app

if __name__ == "__main__":
    app = create_app()

    FLASK_ENV = os.getenv("FLASK_ENV")

    port = 8080
    development_mode = FLASK_ENV == "development"

    print(f"--- Starting web server on port {port}")
    app.run(port=port, host="0.0.0.0", debug=development_mode)
