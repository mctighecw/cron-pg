from seeds import test_users

import sys
import os

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))

import src.models as models
from src.app import create_minimal_app
from src.database import db

app = create_minimal_app()

with app.app_context():
    db.drop_all()
    db.create_all()

    for user in test_users:
        new_user = models.User(**user)
        db.session.add(new_user)

    db.session.commit()
