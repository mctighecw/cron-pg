#!/bin/sh

echo Setting up database...

admin_user=$USER
db_name="cron_pg_db"
db_user="cron_pg_user"
db_password="pw"

echo Creating user: $db_user. Please enter password '"'$db_password'"'
sudo -u $admin_user createuser -P $db_user

echo Closing any open db connections...
sudo -u $admin_user psql -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = $db_name AND pid <> pg_backend_pid();"

echo Dropping old db...
sudo -u $admin_user dropdb $db_name

echo Creating new db...
sudo -u $admin_user createdb -O $db_user -E "utf8" -T "template0" $db_name

echo Initializing db...
FLASK_ENV="development" python $(pwd)/scripts/seed_db.py
