test_users = [
    {
        "email": "john@smith.com",
        "first_name": "John",
        "last_name": "Smith",
        "password": "dev",
        "admin": True
    },
    {
        "email": "susan@williams.com",
        "first_name": "Susan",
        "last_name": "Williams",
        "password": "dev",
        "admin": False
    },
    {
        "email": "sam@jones.com",
        "first_name": "Sam",
        "last_name": "Jones",
        "password": "dev",
        "admin": False
    }
]
