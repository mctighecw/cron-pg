#!/bin/sh

echo Dropping old database and deleting old user...

admin_user=$USER
db_name="cron_pg_db"
db_user="cron_pg_user"

sudo -u $admin_user psql -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '$db_name' AND pid <> pg_backend_pid();"
sudo -u $admin_user dropdb $db_name
sudo -u $admin_user dropuser $db_user
