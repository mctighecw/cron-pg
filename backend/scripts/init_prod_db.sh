#!/bin/sh

echo Initializing production database...

FLASK_ENV="production" python $(pwd)/scripts/seed_db.py
