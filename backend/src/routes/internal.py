from flask import Blueprint, jsonify
from src.constants import default_headers
from src.db_operations import backup_db
from src.email_operations import send_email

internal = Blueprint("internal", __name__)


@internal.route("/backup_db", methods=["GET"])
def run_backup():
    """
    Creates a database dump, uploads file to AWS S3 bucket, and
    sends user an update email.
    """
    try:
        db_name, date_time, success = backup_db()
        send_email(db_name, date_time, success)

        if success:
            res = jsonify({"status": "OK"})
            return res, 200, default_headers
        else:
            res = jsonify({"status": "Error"})
            return res, 409, default_headers
    except:
        res = jsonify({"status": "Error"})
        return res, 409, default_headers
