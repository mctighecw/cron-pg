import logging
import os

from src.env_config import (DB_HOST_PROD, DB_HOST_DEV,
    DB_NAME, DB_USER, DB_PASSWORD)

FLASK_ENV = os.getenv("FLASK_ENV")

if FLASK_ENV == "development":
    LOG_LEVEL = logging.DEBUG
    DB_HOST = DB_HOST_DEV
else:
    LOG_LEVEL = logging.INFO
    DB_HOST = DB_HOST_PROD

SQLALCHEMY_DATABASE_URI = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}"
SQLALCHEMY_TRACK_MODIFICATIONS = False
