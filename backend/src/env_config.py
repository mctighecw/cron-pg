import os
from pathlib import Path
from dotenv import load_dotenv

FLASK_ENV = os.getenv("FLASK_ENV")

if FLASK_ENV == "development":
    root_dir = Path(__file__).parent.parent.parent
    env_file = os.path.join(root_dir, ".env")
    load_dotenv(env_file)

# Database
DB_HOST_PROD = os.getenv("DB_HOST_PROD")
DB_HOST_DEV = os.getenv("DB_HOST_DEV")
DB_NAME = os.getenv("DB_NAME")
DB_USER = os.getenv("DB_USER")
DB_PASSWORD = os.getenv("DB_PASSWORD")

# AWS
AWS_BUCKET = os.getenv("AWS_BUCKET")
AWS_ACCESS_KEY = os.getenv("AWS_ACCESS_KEY")
AWS_SECRET_KEY = os.getenv("AWS_SECRET_KEY")

# Email
SENDER = os.getenv("SENDER")
RECIPIENT = os.getenv("RECIPIENT")
GMAIL_USERNAME = os.getenv("GMAIL_USERNAME")
GMAIL_PASSWORD = os.getenv("GMAIL_PASSWORD")
