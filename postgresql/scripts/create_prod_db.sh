#!/bin/sh

echo Setting up production database...

echo Creating db user...
echo Creating user: $DB_USER. Please enter password '"'$DB_PASSWORD'"'
createuser -U postgres -P $DB_USER

echo Creating new db...
createdb -U postgres -O $DB_USER -E "utf8" -T "template0" $DB_NAME
