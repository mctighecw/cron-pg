# README

This is a template for automating Postgres database backups to the cloud.

There is a Python Flask backend connected to a Postgres database, which has some sample data. The _cron_ service makes an API call to the backend at regular intervals, which dumps the database into a _gzip_ file. This file, which has a custom filename made up of the database name along with a date/time stamp, is then uploaded to a private bucket on [AWS S3](https://aws.amazon.com/s3). Finally, a status email is sent to the user.

## App Information

App Name: cron-pg

Created: January 2020

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/cron-pg)

## Tech Stack

- Python 3.6
- Flask
- PostgreSQL 11.5
- _gzip_
- _cron_ (Linux job scheduler)
- Alpine Linux
- AWS S3
- Gmail
- Docker

## To Run via Docker

1. Make AWS account, set up S3 bucket, set user permissions
2. Make Gmail account and turn on "less secure app access"
3. Add .env file with necessary variables to project root
4. Run commands:

```
$ cd cron-pg
$ source build.sh
$ source init_docker_db.sh
```

Last updated: 2025-02-19
